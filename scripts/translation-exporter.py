import json
import sys

# python3 translation-exporter.py ./translation_05_03_2020.json
# python3 translation-exporter.py ./translation_15_03_2020.json
# python3 translation-exporter.py ./translations_04_06_2020.json
# python3 translation-exporter.py ../translations.json
# python3 translation-exporter.py translations-source.json

if len(sys.argv) != 2:
    print('missing path or lang argument, use python geturllist.py path lang, example: python3 translation-exporter.py ./translation_05_03_2020.json')
    sys.exit(1)

path = str(sys.argv[1])
print(path)

pathComponents = path.split('/')
filename = pathComponents[-1]
lang = 'en'
outputFilename = filename + '-' + lang + '.json' 
with open(path) as json_file:  
    root = json.load(json_file)
    translations = root['translations']
    # print(translations)
    keys = translations.keys()
    export = {}
    newTranslations = {}
    for key in keys:
        if lang in translations[key]:
            keyDict = {}
            keyDict[lang] = translations[key][lang]
            newTranslations[key] = keyDict
        else:
            keyDict = {}
            keyDict["en"] = translations[key]["en"]
            newTranslations[key] = keyDict
    export['translations'] = newTranslations
    with open(outputFilename, 'w', encoding='utf8') as outfile:  
        json.dump(export, outfile, indent=4, ensure_ascii=False)