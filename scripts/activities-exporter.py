import json
import sys

# python3 activities-exporter.py ./activities_15_03_2020.json
# python3 activities-exporter.py ./activities_06_04_2020.json
# python3 activities-exporter.py ../activities.json



def keys_exists(element, *keys):
    '''
    Check if *keys (nested) exists in `element` (dict).
    '''
    if not isinstance(element, dict):
        raise AttributeError('keys_exists() expects dict as first argument.')
    if len(keys) == 0:
        raise AttributeError('keys_exists() expects at least two arguments, one given.')

    _element = element
    for key in keys:
        try:
            _element = _element[key]
        except KeyError:
            return False
    return True

if len(sys.argv) != 2:
    print('missing path or lang argument, use python geturllist.py path lang, example: python3 activities-exporter.py ./activities_15_03_2020.json')
    sys.exit(1)

path = str(sys.argv[1])
print(path)
lang = 'en'
pathComponents = path.split('/')
filename = pathComponents[-1]
outputFilename = filename + '-' + lang + '.json' 
with open(path) as json_file:  
    root = json.load(json_file)
    activities = root['activities']
    # print(activities)
    
    export = {}
    # newActivities = []
    
    for activity in activities:
        if lang in activity['displayName']:
            displayName = activity['displayName'][lang]
            activity['displayName'] = {lang: displayName}
        if lang in activity['verb']:
            verb = activity['verb'][lang]
            activity['verb'] = {lang: verb}
        if lang in activity['pushText']:
            pushText = activity['pushText'][lang]
            activity['pushText'] = {lang: pushText}
        if keys_exists(activity, 'defaultReplyCheersPush', lang):
            defaultReplyCheersPush = activity['defaultReplyCheersPush'][lang]
            activity['defaultReplyCheersPush'] = {lang: defaultReplyCheersPush}
        if keys_exists(activity, 'defaultReplyCheersButton', lang):
            defaultReplyCheersButton = activity['defaultReplyCheersButton'][lang]
            activity['defaultReplyCheersButton'] = {lang: defaultReplyCheersButton}
        if keys_exists(activity, 'defaultReplyStayPush', lang):
            defaultReplyStayPush = activity['defaultReplyStayPush'][lang]
            activity['defaultReplyStayPush'] = {lang: defaultReplyStayPush}
        if keys_exists(activity, 'defaultReplyStayButton', lang):
            defaultReplyStayButton = activity['defaultReplyStayButton'][lang]
            activity['defaultReplyStayButton'] = {lang: defaultReplyStayButton}
        
    export['activities'] = activities
    with open(outputFilename, 'w', encoding='utf8') as outfile:  
        json.dump(export, outfile, indent=4, ensure_ascii=False)