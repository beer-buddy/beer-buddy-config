import json
import sys

# python3 translation-importer.py ./translations_20_03_2020.json ./translations_to_import.json no
# python3 translation-importer.py ./translations_20_03_2020.json ./translations_to_import.json se
# python3 translation-importer.py ./translations_20_03_2020.json ./translations_to_import.json fi
# python3 translation-importer.py ../translations.json ../se/translations_04_06_2020.json-se.json se
# python3 translation-importer.py ../translations.json ../pl/translations.json-en-PL.json pl


if len(sys.argv) != 4:
    print('missing parameter, example: python3 translations-importer.py ./translations_20_03_2020.json ./translations_to_import.json no')
    sys.exit(1)

sourcePath = str(sys.argv[1])
print(sourcePath)

toMergePath = str(sys.argv[2])
print(toMergePath)

lang = str(sys.argv[3])

pathComponents = sourcePath.split('/')
filename = pathComponents[-1]
outputFilename = filename + '-' + lang +'.json' 

outPut = {}
with open(sourcePath) as source_file:
    with open(toMergePath) as merge_file:
        source = json.load(source_file)
        # print(source)
        merge = json.load(merge_file)
        # print(merge)
        translations = source['translations']
        # print(activities)
        mergeTranslations = merge['translations']
        
        keys = translations.keys()

        for key in keys:
            if key in mergeTranslations:
                newTranslation = mergeTranslations[key][lang]
                if newTranslation:
                    translations[key][lang] = newTranslation

        outPut = source

with open(outputFilename, 'w', encoding='utf8') as outfile:  
    json.dump(outPut, outfile, indent=4, ensure_ascii=False)