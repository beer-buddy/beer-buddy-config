import json
import sys

# python3 activities-importer.py ./activities_20_03_2020.json ./no_activities_20_03_2020.json no
# python3 activities-importer.py ./activities_20_03_2020.json ./fi_activities_20_03_2020.json fi
# python3 activities-importer.py ./activities_20_03_2020.json ./activities_to_import.json se
# python3 activities-importer.py ../activities.json ../se/activities_06_04_2020-se.json se
# python3 activities-importer.py ../activities.json ../pl/activities.json-en-PL.json pl

def keys_exists(element, *keys):
    '''
    Check if *keys (nested) exists in `element` (dict).
    '''
    if not isinstance(element, dict):
        raise AttributeError('keys_exists() expects dict as first argument.')
    if len(keys) == 0:
        raise AttributeError('keys_exists() expects at least two arguments, one given.')

    _element = element
    for key in keys:
        try:
            _element = _element[key]
        except KeyError:
            return False
    return True

if len(sys.argv) != 4:
    print('missing parameter, example: python3 activities-importer.py ./activities_15_03_2020.json ./activities_no.json')
    sys.exit(1)

sourcePath = str(sys.argv[1])
print(sourcePath)

toMergePath = str(sys.argv[2])
print(toMergePath)

lang = str(sys.argv[3])

pathComponents = sourcePath.split('/')
filename = pathComponents[-1]
outputFilename = filename + '-' + lang +'.json' 

outPut = {}
with open(sourcePath) as source_file:
    with open(toMergePath) as merge_file:
        source = json.load(source_file)
        # print(source)
        merge = json.load(merge_file)
        # print(merge)
        activities = source['activities']
        # print(activities)
        mergeActivities = merge['activities']
    
        for i, activity in enumerate(activities):
            mergeActivity = mergeActivities[i]
            if keys_exists(mergeActivity, 'displayName', lang):
                newDisplayName = mergeActivity['displayName'][lang]
                if newDisplayName:
                    displayName = activity['displayName']
                    displayName[lang] = newDisplayName
            if keys_exists(mergeActivity, 'verb', lang):
                newVerb = mergeActivity['verb'][lang]
                if newVerb:
                    verb = activity['verb']
                    verb[lang] = newVerb
            if keys_exists(mergeActivity, 'pushText', lang):
                newPushText = mergeActivity['pushText'][lang]
                if newPushText:
                    pushText = activity['pushText']
                    pushText[lang] = newPushText
        outPut = source

with open(outputFilename, 'w', encoding='utf8') as outfile:  
    json.dump(outPut, outfile, indent=4, ensure_ascii=False)